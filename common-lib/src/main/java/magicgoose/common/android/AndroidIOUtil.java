package magicgoose.common.android;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import magicgoose.common.function.Func1Ex;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class AndroidIOUtil {
    @SuppressLint("NewApi")
    public static <T> T readRawResource(final Context c, final int resId, final Func1Ex<Reader, T> readFun) {
        try (final InputStream is = c.getResources().openRawResource(resId)) {
            final BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            final T result = readFun.call(reader);
            return result;
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    public static List<String> getStrings(final Context context, final int[] ids) {
        final Resources resources = context.getResources();
        final ArrayList<String> result = new ArrayList<>(ids.length);
        for (final int id : ids) {
            result.add(resources.getString(id));
        }
        return result;
    }
}
