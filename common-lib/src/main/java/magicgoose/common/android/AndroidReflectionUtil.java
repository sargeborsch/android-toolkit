package magicgoose.common.android;

import android.view.View;
import magicgoose.common.util.IterableTools;
import magicgoose.common.util.ReflectionUtil;

import java.lang.reflect.Field;

public class AndroidReflectionUtil {

    public static <T> void setViewsToNull(T fragment, Class<T> baseClass) {
        // noinspection unchecked
        final Class<T> fragmentClass = (Class<T>) fragment.getClass();

        final Iterable<Field> allFields = ReflectionUtil.getDeclaredAndSubclassFields(fragmentClass, baseClass);
        final Iterable<Field> viewFields = IterableTools.filter(allFields, f -> View.class.isAssignableFrom(f.getType()));

        for (final Field f : viewFields) {
            f.setAccessible(true);
            try {
                f.set(fragment, null);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                throw new Error(e);
            }
        }
    }
}
