package magicgoose.common.android;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.CharacterStyle;

public class FormatUtil {

    public static Spanned getStyledString(final String src, final CharacterStyle... styles) {
        final SpannableString ss = new SpannableString(src);
        final int length = ss.length();
        for (final CharacterStyle style : styles) {
            ss.setSpan(style, 0, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return ss;
    }
}
