package magicgoose.common.android;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Pair;
import magicgoose.common.util.WebUtil;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GooglePlayUpdateChecker {

    private static Message message = null;

    public static final String GooglePlayUrlPrefix = "play.google.com/store/apps/details?id=";
    private static final String PrefsKey = "GooglePlayUpdateChecker";
    private static final String KeyLastCheckTime = "KeyLastCheckTime";
    private static final Pattern pattern = Pattern.compile("<div[^>]*?\\w+=\"softwareVersion\">\\s*(\\S*)\\s*</div>",
            Pattern.CASE_INSENSITIVE);
    private static final long UpdateCheckInterval = 1000 * 60 * 60 * 3; // hours
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();
    private volatile Activity activity;
    private volatile boolean running = false;

    private final Context applicationContext;

    public GooglePlayUpdateChecker(final Context applicationContext) {
        this.applicationContext = applicationContext;
    }


    public static String getVersionFromGooglePlay(String packageName) throws IOException, InterruptedException {

        final String url = GooglePlayUrlPrefix + packageName;
        final String page = WebUtil.downloadContentHTTPS(url);

        if (Thread.interrupted())
            throw new InterruptedException();

        final Matcher matcher = pattern.matcher(page);
        if (matcher.find()) {
            final String version = matcher.group(1);
            return version;
        } else {
            throw new IOException();
        }
    }

    public void detachActivity() {
        activity = null;
    }

    public <T extends Activity & IMessageHandler> void checkUpdates(T activity) {
        if (running || showSavedMessage(activity))
            return;

        running = true;
        this.activity = activity;

        try {
            checkUpdatesUnsafe();
        } catch (Exception e) {
            e.printStackTrace();
            running = false;
        }
    }

    private boolean showSavedMessage(Activity activity) {
        if (message != null && activity != null) {
            showMessage(activity, message);
            message = null;
            return true;
        }
        return false;
    }

    private void showMessage(Activity activity, Message message) {
        ((IMessageHandler) activity).showMessage(message);
    }

    private void checkUpdatesUnsafe() throws Exception {
        final Pair<String, String> packageAndVersion = getPackageNameAndVersionForRequest();
        if (packageAndVersion == null) return;

        startRequest(packageAndVersion.first, packageAndVersion.second);
    }

    private Pair<String, String> getPackageNameAndVersionForRequest() throws PackageManager.NameNotFoundException {
        final PackageManager pm = activity.getPackageManager();
        final String packageName = activity.getPackageName();
        final PackageInfo packageInfo = pm.getPackageInfo(packageName, 0);
        final String currentVersionName = packageInfo.versionName.trim();

        final long currentTime = System.currentTimeMillis();
        final long lastCheckTime = getSharedPreferences().getLong(KeyLastCheckTime, 0);

        if (currentTime - lastCheckTime < UpdateCheckInterval) {
            running = false;
            return null;
        }
        return Pair.create(packageName, currentVersionName);
    }

    private void startRequest(final String packageName, final String currentVersionName) {
        executorService.submit(() -> {
            try {
                final String versionFromGooglePlay = getVersionFromGooglePlay(packageName);
                SharedHandler.post(() -> {
                    handleResult(currentVersionName, versionFromGooglePlay);
                    running = false;
                });
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                running = false;
            }
        });
    }

    private SharedPreferences getSharedPreferences() {
        return applicationContext.getSharedPreferences(PrefsKey, Context.MODE_PRIVATE);
    }

    private void handleResult(String currentVersionName, String versionFromGooglePlay) {
        getSharedPreferences().edit()
                .putLong(KeyLastCheckTime, System.currentTimeMillis())
                .apply();

        if (currentVersionName.equals(versionFromGooglePlay))
            return;

        message = new Message(currentVersionName, versionFromGooglePlay);
        if (activity != null) {
            showSavedMessage(activity);
        }
    }

    public static class Message {
        public final String currentVersion;
        public final String newVersion;

        public Message(String currentVersion, String newVersion) {
            this.currentVersion = currentVersion;
            this.newVersion = newVersion;
        }
    }

    public interface IMessageHandler {
        void showMessage(Message m);
    }
}
