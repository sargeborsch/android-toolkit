package magicgoose.common.android;

import android.os.Handler;
import android.os.Looper;

@SuppressWarnings("UnusedReturnValue")
public class SharedHandler {

    private final static Handler handler = new Handler(Looper.getMainLooper());

    public static boolean post(Runnable r) {
        return handler.post(r);
    }

    public static boolean postAtTime(Runnable r, long uptimeMillis) {
        return handler.postAtTime(r, uptimeMillis);
    }

    public static boolean postAtTime(Runnable r, Object token, long uptimeMillis) {
        return handler.postAtTime(r, token, uptimeMillis);
    }

    public static boolean postDelayed(Runnable r, long delayMillis) {
        return handler.postDelayed(r, delayMillis);
    }

    public static boolean postAtFrontOfQueue(Runnable r) {
        return handler.postAtFrontOfQueue(r);
    }

    public static void removeCallbacks(Runnable r) {
        handler.removeCallbacks(r);
    }

    public static void removeCallbacks(Runnable r, Object token) {
        handler.removeCallbacks(r, token);
    }
}
