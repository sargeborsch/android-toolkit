package magicgoose.common.android;

import android.text.Editable;
import android.text.TextWatcher;
import rx.functions.Action1;

public class SimpleTextWatcher implements TextWatcher {

    private final Action1<CharSequence> handler;

    public SimpleTextWatcher(final Action1<CharSequence> handler) {
        this.handler = handler;
    }

    @Override
    public void beforeTextChanged(final CharSequence charSequence, final int i, final int i1, final int i2) {
    }

    @Override
    public void onTextChanged(final CharSequence charSequence, final int i, final int i1, final int i2) {
        handler.call(charSequence);
    }

    @Override
    public void afterTextChanged(final Editable editable) {
    }

}
