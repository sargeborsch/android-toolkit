package magicgoose.common.android;

import android.text.TextUtils;
import magicgoose.common.util.ListTools;

import java.util.ArrayList;

public class TextUtil {

    public static CharSequence join(Iterable<CharSequence> parts, CharSequence sep) {

        final ArrayList<CharSequence> allParts = new ArrayList<>();

        boolean first = true;
        for (final CharSequence part : parts) {
            if (!first) {
                allParts.add(sep);
            } else {
                first = false;
            }

            allParts.add(part);
        }

        return TextUtils.concat(ListTools.toArray(allParts, CharSequence.class));
    }

}
