package magicgoose.common.android;

import android.view.View;
import android.view.ViewGroup;
import magicgoose.common.AbstractReadOnlyList;
import magicgoose.common.util.IterableTools;

import java.util.Collections;
import java.util.List;

public class ViewUtil {
    @SuppressWarnings("unchecked")
    public static <T extends View> List<T> getChildren(ViewGroup vg) {
        return new AbstractReadOnlyList<>(
                vg.getChildCount(),
                (i) -> (T) vg.getChildAt(i));
    }

    public static void setVisibleOrGone(View view, boolean isVisible) {
        view.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    public static Iterable<View> getAllChildrenRecursive(View v) {
        final List<View> thisViewPart = Collections.singletonList(v);
        if (v instanceof ViewGroup) {
            final List<View> children = magicgoose.common.android.ViewUtil.getChildren((ViewGroup) v);
            return IterableTools.concat(
                    thisViewPart,
                    IterableTools.flatMap(children, ViewUtil::getAllChildrenRecursive));
        }

        return thisViewPart;
    }
}
