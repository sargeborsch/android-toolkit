package magicgoose.common.android.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class BaseViewHolder<TData, TView extends View> extends RecyclerView.ViewHolder {

    private int index = -1;

    protected BaseViewHolder(final TView itemView) {
        super(itemView);
    }

    protected TView getView() {
        //noinspection unchecked
        return (TView) itemView;
    }

    public abstract void updateWithData(TData data);

    public int getIndex() {
        return index;
    }

    public void setIndex(final int index) {
        this.index = index;
    }
}
