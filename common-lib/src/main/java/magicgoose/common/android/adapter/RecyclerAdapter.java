package magicgoose.common.android.adapter;

import android.support.v7.widget.RecyclerView;
import rx.Observable;
import rx.subjects.PublishSubject;

import java.util.List;

public abstract class RecyclerAdapter<TData, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    protected final PublishSubject<Integer> itemClickSubject = PublishSubject.create();
    protected final PublishSubject<TData> itemClickDataSubject = PublishSubject.create();

    public final Observable<Integer> itemClicks = itemClickSubject;
    public final Observable<TData> itemClicksData = itemClickDataSubject;

    public abstract void setItems(List<TData> newItems);
}
