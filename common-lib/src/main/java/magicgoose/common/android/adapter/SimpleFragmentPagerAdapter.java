package magicgoose.common.android.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {
    private final List<Class<? extends Fragment>> fragmentClasses;
    private final List<String> fragmentNames;

    public SimpleFragmentPagerAdapter(
            final FragmentManager fm,
            final List<Class<? extends Fragment>> fragmentClasses,
            final List<String> fragmentNames
    ) {
        super(fm);
        this.fragmentClasses = fragmentClasses;
        this.fragmentNames = fragmentNames;
    }

    @Override
    public Fragment getItem(final int position) {
        try {
            return fragmentClasses.get(position).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Error(e);
        }
    }

    @Override
    public CharSequence getPageTitle(final int position) {
        return fragmentNames.get(position);
    }

    @Override
    public int getCount() {
        return fragmentClasses.size();
    }
}
