package magicgoose.common.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import rx.functions.Func1;

import java.util.Collections;
import java.util.List;

public class SimpleRecyclerAdapter<TData, TView extends View, VH extends BaseViewHolder<TData, TView>> extends RecyclerAdapter<TData, VH> implements View.OnClickListener {

    private final LayoutInflater inflater;
    private final int layoutId;
    private final Func1<TView, VH> vhFactory;

    private List<TData> items = Collections.emptyList();

    public static <TData, TView extends View, VH extends BaseViewHolder<TData, TView>> SimpleRecyclerAdapter<TData, TView, VH> create(final Context context, final int layoutId, final Func1<TView, VH> vhFactory) {
        return new SimpleRecyclerAdapter<>(context, layoutId, vhFactory);
    }


    protected SimpleRecyclerAdapter(final Context context, final int layoutId, final Func1<TView, VH> vhFactory) {
        this.layoutId = layoutId;
        this.vhFactory = vhFactory;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void setItems(List<TData> newItems) {
        items = newItems;
        notifyDataSetChanged();
    }

    @Override
    public VH onCreateViewHolder(final ViewGroup viewGroup, final int i) {
        //noinspection unchecked
        final TView view = (TView) inflater.inflate(layoutId, viewGroup, false);
        final VH vh = vhFactory.call(view);
        view.setTag(vh);
        view.setOnClickListener(this);
        return vh;
    }

    @Override
    public void onBindViewHolder(final VH vh, final int i) {
        vh.setIndex(i);
        vh.updateWithData(items.get(i));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onClick(final View view) {
        //noinspection unchecked
        final VH vh = (VH) view.getTag();
        final int clickedIndex = vh.getIndex();
        itemClickSubject.onNext(clickedIndex);

        final TData item = items.get(clickedIndex);
        itemClickDataSubject.onNext(item);
    }
}
