package magicgoose.common.android.adapter;

import android.widget.TextView;

public class StringViewHolder extends BaseViewHolder<CharSequence, TextView> {

    public StringViewHolder(final TextView itemView) {
        super(itemView);
    }

    @Override
    public void updateWithData(final CharSequence s) {
        getView().setText(s);
    }
}
