package magicgoose.common.android.rx;

import android.support.v7.app.AppCompatActivity;
import rx.Observable;
import rx.functions.Action1;

public abstract class ReactiveActivity extends AppCompatActivity {

    private final RxHelper rxHelper = new RxHelper();

    @Override
    protected void onPause() {
        super.onPause();
        rxHelper.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        rxHelper.onDestroy();
    }

    public <T> void subscribeUntilPause(String id, Observable<T> observable, Action1<T> handler) {
        rxHelper.subscribeUntilPause(id, observable, handler);
    }

    public <T> void subscribeUntilDestroy(Observable<T> observable, Action1<T> handler) {
        rxHelper.subscribeUntilDestroy(observable, handler);
    }

    public <T> void subscribeUntilPause(Observable<T> observable, Action1<T> handler) {
        rxHelper.subscribeUntilPause(observable, handler);
    }

    public <T> void subscribeUntilDestroy(String id, Observable<T> observable, Action1<T> handler) {
        rxHelper.subscribeUntilDestroy(id, observable, handler);
    }
}

