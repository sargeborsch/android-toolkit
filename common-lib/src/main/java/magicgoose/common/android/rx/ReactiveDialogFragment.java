package magicgoose.common.android.rx;

import android.support.v4.app.DialogFragment;
import rx.Observable;
import rx.functions.Action1;

public class ReactiveDialogFragment extends DialogFragment {

    private final RxHelperFragment rxHelperFragment = new RxHelperFragment();

    @Override
    public void onDestroy() {
        super.onDestroy();
        rxHelperFragment.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        rxHelperFragment.onDestroyView();
    }

    @Override
    public void onPause() {
        super.onPause();
        rxHelperFragment.onPause();
    }

    public <T> void subscribeUntilPause(Observable<T> observable, Action1<T> handler) {
        rxHelperFragment.subscribeUntilPause(observable, handler);
    }
    public <T> void subscribeUntilPause(String id, Observable<T> observable, Action1<T> handler) {
        rxHelperFragment.subscribeUntilPause(id, observable, handler);
    }

    public <T> void subscribeUntilViewDestroy(Observable<T> observable, Action1<T> handler) {
        rxHelperFragment.subscribeUntilViewDestroy(observable, handler);
    }

    public <T> void subscribeUntilViewDestroy(String id, Observable<T> observable, Action1<T> handler) {
        rxHelperFragment.subscribeUntilViewDestroy(id, observable, handler);
    }

    public <T> void subscribeUntilDestroy(Observable<T> observable, Action1<T> handler) {
        rxHelperFragment.subscribeUntilDestroy(observable, handler);
    }

    public <T> void subscribeUntilDestroy(String id, Observable<T> observable, Action1<T> handler) {
        rxHelperFragment.subscribeUntilDestroy(id, observable, handler);
    }
}

