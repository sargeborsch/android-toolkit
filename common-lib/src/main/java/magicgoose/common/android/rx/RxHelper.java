package magicgoose.common.android.rx;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RxHelper {
    private final List<Subscription> subscriptionsBeforeDestroy = new ArrayList<>();
    private final List<Subscription> subscriptionsBeforePause = new ArrayList<>();

    private final HashMap<String, Subscription> namedSubscriptionsBeforeDestroy = new HashMap<>();
    private final HashMap<String, Subscription> namedSubscriptionsBeforePause = new HashMap<>();

    public <T> void subscribeUntilDestroy(Observable<T> observable, Action1<T> handler) {
        subscribeUnnamed(subscriptionsBeforeDestroy, observable, handler);
    }

    public <T> void subscribeUntilPause(Observable<T> observable, Action1<T> handler) {
        subscribeUnnamed(subscriptionsBeforePause, observable, handler);
    }

    public <T> void subscribeUntilDestroy(String id, Observable<T> observable, Action1<T> handler) {
        subscribeNamed(namedSubscriptionsBeforeDestroy, id, observable, handler);
    }

    public <T> void subscribeUntilPause(String id, Observable<T> observable, Action1<T> handler) {
        subscribeNamed(namedSubscriptionsBeforePause, id, observable, handler);
    }

    protected <T> void subscribeUnnamed(List<Subscription> where, Observable<T> observable, Action1<T> handler) {
        where.add(RxUtilAndroid.subscribeOnMainThread(observable, handler));
    }

    protected <T> void subscribeNamed(Map<String, Subscription> where, String id, Observable<T> observable, Action1<T> handler) {
        cancelSubscription(id, where);
        subscribeWith(id, observable, handler, where);
    }

    protected <T> void subscribeWith(final String id, final Observable<T> observable, final Action1<T> handler, final Map<String, Subscription> subscriptionMap) {
        final Subscription subscription = RxUtilAndroid.subscribeOnMainThread(observable, handler);
        namedSubscriptionsBeforeDestroy.put(id, subscription);
    }

    protected void cancelSubscription(final String id, final Map<String, Subscription> subscriptionMap) {
        if (subscriptionMap.containsKey(id)) {
            final Subscription removed = subscriptionMap.remove(id);
            removed.unsubscribe();
        }
    }

    protected void unsubscribeAll(final List<Subscription> subscriptions) {
        for (Subscription s : subscriptions) {
            s.unsubscribe();
        }
        subscriptions.clear();
    }

    protected void unsubscribeAll(final Map<String, Subscription> subscriptionMap) {
        for (final Subscription subscription : subscriptionMap.values()) {
            subscription.unsubscribe();
        }
        subscriptionMap.clear();
    }


    public void onDestroy() {
        unsubscribeAll(subscriptionsBeforeDestroy);
        unsubscribeAll(namedSubscriptionsBeforeDestroy);
    }

    public void onPause() {
        unsubscribeAll(subscriptionsBeforePause);
        unsubscribeAll(namedSubscriptionsBeforePause);
    }
}
