package magicgoose.common.android.rx;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RxHelperFragment extends RxHelper {
    private final List<Subscription> subscriptionsBeforeViewDestroy = new ArrayList<>();

    private final HashMap<String, Subscription> namedSubscriptionsBeforeViewDestroy = new HashMap<>();


    public <T> void subscribeUntilViewDestroy(Observable<T> observable, Action1<T> handler) {
        subscribeUnnamed(subscriptionsBeforeViewDestroy, observable, handler);
    }

    public <T> void subscribeUntilViewDestroy(String id, Observable<T> observable, Action1<T> handler) {
        subscribeNamed(namedSubscriptionsBeforeViewDestroy, id, observable, handler);
    }

    public void onDestroyView() {
        unsubscribeAll(subscriptionsBeforeViewDestroy);
        unsubscribeAll(namedSubscriptionsBeforeViewDestroy);
    }
}
