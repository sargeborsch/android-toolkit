package magicgoose.common.android.rx;


import magicgoose.common.android.SharedHandler;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;

public class RxUtilAndroid {

    public static <T> Subscription subscribeOnMainThread(Observable<T> observable, Action1<T> onNext) {
        return observable.subscribe(x ->
                SharedHandler.post(() -> onNext.call(x)));
    }

}
