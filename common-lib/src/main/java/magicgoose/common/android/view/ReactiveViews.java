package magicgoose.common.android.view;

import android.widget.CompoundButton;
import rx.Observable;
import rx.subjects.BehaviorSubject;

public class ReactiveViews {
    public static Observable<Boolean> getCheckedObservable(CompoundButton cb) {

        final BehaviorSubject<Boolean> result = BehaviorSubject.create(cb.isChecked());

        cb.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            result.onNext(isChecked);
        });

        return result;
    }
}
